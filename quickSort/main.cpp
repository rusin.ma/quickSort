//
//  main.cpp
//  quickSort
//
//  Created by Marta Rusin on 06/11/2022.
//

#include <iostream>
using namespace std;
void qsort(int*, int, int);

int main() {
    // warunek rekurencji g>d
    
    cout << "Podaj wielkosc tablicy: "<< endl;
    int n;
    cin >> n;
    int* T = new int[n];
    
    for (int i=0; i< n; i++) {
        T[i] = rand();
    }
    


    cout << "Tablica nieposortowana: "<< endl;
    for(int i = 0; i < n; i++) {
        cout << T[i] << " ";
    }
    int d = 0;
    int g = n-1;
    qsort(T, d, g);
    
    cout << "Tablica posortowana: "<< endl;
    for(int i = 0; i < n; i++) {
        cout << T[i] << " ";
    }
    
    return 0;
};

void qsort(int* T, int d, int g) {
    //w2, str 7, 8
    int s=d;
    if (g> d) {
        for(int i=d+1; i<=g; i++) {
            
            if(T[i] < T[d]) {
                s=s+1;
                swap(T[s],T[i]);
            }
        }
        swap(T[d], T[s]);
        qsort(T, d, s-1);
        qsort(T, s+1, g);
    }
};
